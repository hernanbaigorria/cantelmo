
    <?php require_once('includes/head.php'); ?>

  <body>

    <?php require_once('includes/nav.html'); ?>

    <header class="header-internas">
      <div class="container text-center">
        <h3>Servicio</h3>
      </div>
    </header>

    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-2"></div>
        <div class="col-xs-12 col-sm-8 text-servicios text-center">
          <h3>Área <br>
            <strong>Previsional</strong></h3>
            <ul>
              <li>Liquidación de remuneraciones y cargas sociales, inscripción en los organismos previsionales correspondientes, registro en los libros exigidos por las normas vigentes, preparación de la información para registraciones contables, atención de inspecciones, asesoramiento sobre cuestiones previsionales, cálculo de indemnizaciones por desvinculaciones.</li>
            </ul>
        </div>
        <div class="col-xs-12 col-sm-2"></div>
      </div>
    </div>

    <div class="col-xs-12 col-sm-12 padding-0 back-home-serv" style="background: #005883;padding: 60px 0;">
      <div class="container text-center">
        <div class="row">
          <div class="col-xs-12 col-sm-1"></div>
          <div class="col-xs-12 col-sm-2">
            <h5>Área</h5>
            <h6>Impositiva</h6>
            <a href="area-impositiva.php" class="border-enlace">+</a>
          </div>
          <div class="col-xs-12 col-sm-2">
            <h5>Área</h5>
            <h6>Contable</h6>
            <a href="area-contable.php" class="border-enlace">+</a>
          </div>
          <div class="col-xs-12 col-sm-4">
            <h5>Outsourcing</h5>
            <h6>Administrativo Contable</h6>
            <a href="outsourcing-administrativo-contable.php" class="border-enlace">+</a>
          </div>
          <div class="col-xs-12 col-sm-2">
            <h5>Área</h5>
            <h6>Auditoría</h6>
            <a href="area-auditoria.php" class="border-enlace">+</a>
          </div>
          <div class="col-xs-12 col-sm-1"></div>
        </div>
      </div>
    </div>

  <?php require_once('includes/footer_home.html'); ?>

