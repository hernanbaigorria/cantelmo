
    <?php require_once('includes/head.php'); ?>

  <body>

    <?php require_once('includes/nav.html'); ?>

    <header class="header-internas">
      <div class="container text-center">
        <h3>Servicio</h3>
      </div>
    </header>

    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-2"></div>
        <div class="col-xs-12 col-sm-8 text-servicios text-center">
          <h3>Outsourcing<br>
            <strong>Administrativo Contable</strong></h3>
            <ul>
              <li>A través de la tercerización de las tareas administrativas más comunes (facturación, registro de cobranzas, recepción de facturas de compras y su correspondiente pago, registraciones contables y reportes mensuales y anuales de gestión) permite a la empresa concentrar su capital financiero y humano en su “Core Business” y así lograr mayor eficiencia en sus procesos.</li>
            </ul>
        </div>
        <div class="col-xs-12 col-sm-2"></div>
      </div>
    </div>

    <div class="col-xs-12 col-sm-12 padding-0 back-home-serv" style="background: #005883;padding: 60px 0;">
      <div class="container text-center">
        <div class="row">
          <div class="col-xs-12 col-sm-2"></div>
          <div class="col-xs-12 col-sm-2">
            <h5>Área</h5>
            <h6>Impositiva</h6>
            <a href="area-impositiva.php" class="border-enlace">+</a>
          </div>
          <div class="col-xs-12 col-sm-2">
            <h5>Área</h5>
            <h6>Contable</h6>
            <a href="area-contable.php" class="border-enlace">+</a>
          </div>
          <div class="col-xs-12 col-sm-2">
            <h5>Área</h5>
            <h6>Previsional</h6>
            <a href="area-previsional.php" class="border-enlace">+</a>
          </div>
          <div class="col-xs-12 col-sm-2">
            <h5>Área</h5>
            <h6>Auditoría</h6>
            <a href="area-auditoria.php" class="border-enlace">+</a>
          </div>
          <div class="col-xs-12 col-sm-2"></div>
        </div>
      </div>
    </div>

  <?php require_once('includes/footer_home.html'); ?>

