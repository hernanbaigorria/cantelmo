
    <?php require_once('includes/head.php'); ?>

  <body>

    <?php require_once('includes/nav.html'); ?>

    <header class="header-internas">
      <div class="container text-center">
        <h3>Links de interés</h3>
      </div>
    </header>

    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-2"></div>
        <div class="col-xs-12 col-sm-8 text-links">
          <p>
            Constancia de Cuil <br><br>

          <strong>Superintendencia de servicios de salud</strong><br>
          Superintendencia de servicios de salud<br>
          Consulta al padron sobre la obra social que se encuentra inscripto/a<br>
          Obras sociales para la atención del personal de servicio domestico<br>
          Obras sociales para la atención de monotributistas<br>
          Páginas web de las Obras Sociales<br><br>

          <strong>AFIP</strong><br> 
          AFIP<br>
          Busqueda de agencias AFIP<br> 
          Formularios AFIP<br>
          Consultas, presentaciones, ABM de impuestos, etc. que requieran clave fiscal<br>
          Constancia de inscripción AFIP<br>
          Vencimientos<br>
          Calculo de intereses AFIP<br>
          Consultas de agentes fiscales<br>
          Verificación de validez de comprobantes emitidos<br>
          Consulta de entidades exentas<br><br>

          <strong>Banco Central de la República Argentina</strong><br> 
          Banco Central de la Republica Argentina<br><br> 

          <strong>Fiscos Provinciales</strong><br>
          Ciudad de Buenos Aires<br>
          Provincia de Buenos Aires<br><br>

          <strong>Comisión Arbitral</strong><br>
          <a href="http://www.ca.gov.ar" target="_blank">http://www.ca.gov.ar</a><br><br>

          <strong>Sircreb</strong><br>
          <a href="http://www.sircreb.gov.ar/" target="_blank">http://www.sircreb.gov.ar/</a><br><br>

          <strong>Aduana</strong><br>
          <a href="http://www.aduananews.com.ar/" target="_blank">http://www.aduananews.com.ar/</a><br><br>

          <strong>Infoleg</strong><br>
          <a href="http://infoleg.mecon.gov.ar/" target="_blank">http://infoleg.mecon.gov.ar/</a><br><br>

          <strong>IGJ</strong><br>
          <a href="http://www.jus.gov.ar/registros/IGJ/" target="_blank">http://www.jus.gov.ar/registros/IGJ/</a><br><br>

          <strong>Consejos</strong><br>
          <a href="http://www.consejo.org.ar/" target="_blank">http://www.consejo.org.ar/</a><br>
          <a href="http://www.cpba.com.ar/New/Index.html" target="_blank">http://www.cpba.com.ar/New/Index.html</a>

          </p>
        </div>
        <div class="col-xs-12 col-sm-2"></div>
      </div>
    </div>

  <?php require_once('includes/footer_home.html'); ?>

