
    <?php require_once('includes/head.php'); ?>

  <body>

    <?php require_once('includes/nav.html'); ?>

    <header class="header-home">
      <div class="container text-center">
        <h3>Asesoramiento integral <strong>a pequeñas y medianas empresas</strong> <br>en las áreas: <strong>contable, impositiva, previsional, societaria y de auditoría.</strong></h3>
      </div>
    </header>

    <!-- Page Content -->

    <div class="col-xs-12 col-sm-12 padding-0 boxes-text-home">
      
      <div class="row">
        <div class="col-xs-12 col-sm-6 padding-0">
          <div style="background:url('img/C_home_box_left_01.jpg');height:27rem;background-size:cover;background-position:center;background-repeat:no-repeat;">
          </div>
        </div>
        <div class="col-xs-12 col-sm-6 box-text">
          <p>Nuestro principal objetivo es brindarle a nuestros clientes un servicio de excelencia acorde a sus necesidades.</p>
          <a href="#" class="btn-home">NOSOTROS</a>
        </div>
      </div>

      <div class="row">
        <div class="col-xs-12 col-sm-6 box-text" style="align-items: flex-end;text-align: right;">
          <p>Las bases del estudio son las mismas que aplicamos para nuestros clientes</p>
          <a href="#" class="btn-home">CLIENTES</a>
        </div>
        <div class="col-xs-12 col-sm-6 padding-0">
          <div style="background:url('img/C_home_box_right_01.jpg');height:27rem;background-size:cover;background-position:center;background-repeat:no-repeat;">
          </div>
        </div>
      </div>

    </div>

    <div class="col-xs-12 col-sm-12 padding-0 back-home-serv" id="servicios">
      <div class="container text-center">
        <div class="row">
          <div class="col-xs-12 col-sm-12">
            <h3>Servicios</h3>
            <hr>
          </div>
          <div class="col-xs-12 col-sm-2">
            <h5>Área</h5>
            <h6>Impositiva</h6>
            <a href="area-impositiva.php" class="border-enlace">+</a>
          </div>
          <div class="col-xs-12 col-sm-2">
            <h5>Área</h5>
            <h6>Contable</h6>
            <a href="area-contable.php" class="border-enlace">+</a>
          </div>
          <div class="col-xs-12 col-sm-4">
            <h5>Outsourcing</h5>
            <h6>Administrativo Contable</h6>
            <a href="outsourcing-administrativo-contable.php" class="border-enlace">+</a>
          </div>
          <div class="col-xs-12 col-sm-2">
            <h5>Área</h5>
            <h6>Previsional</h6>
            <a href="area-previsional.php" class="border-enlace">+</a>
          </div>
          <div class="col-xs-12 col-sm-2">
            <h5>Área</h5>
            <h6>Auditoría</h6>
            <a href="area-auditoria.php" class="border-enlace">+</a>
          </div>
        </div>
      </div>
    </div>
    
    <div class="col-xs-12 col-sm-12 padding-0 back-home-links">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-12 text-center">
            <p>Links de interés</p>
            <a href="#" class="border-enlace" style="color:#033751;border-color:#033751;">+</a>
          </div>
        </div>
      </div>
    </div>

  <?php require_once('includes/footer_home.html'); ?>

