
    <?php require_once('includes/head.php'); ?>

  <body>

    <?php require_once('includes/nav.html'); ?>

    <header class="header-internas">
      <div class="container text-center">
        <h3>Nosotros</h3>
      </div>
    </header>

    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-1"></div>
        <div class="col-xs-12 col-sm-10 text-nostros">
          <p>
            Nuestro principal objetivo es brindarle a nuestros clientes un servicio de excelencia acorde a sus necesidades y que puedan encontrar en nosotros una total satisfacción a los problemas que les puedan surgir en la vida de los negocios.
          </p>
          <h3>Valores</h3>
          <p style="margin-top: 15px;">Las bases sobre las cuales se organizó el estudio, son las mismas que aplicamos para la atención de nuestros clientes:</p>
          <div class="row">
            <div class="col-xs-12 col-sm-4 text-center">
              <h4>Profesionalismo</h4>
              <h5>Experiencia, responsabilidad, capacidad y constante actualización, nos permiten brindar un servicio adecuado.</h5>
              <h6>•</h6>
            </div>
            <div class="col-xs-12 col-sm-4 text-center">
              <h4>Vocación</h4>
              <h5>Celeridad en la respuesta a los requerimientos y rápida resolución de los problemas.</h5>
              <h6>•</h6>
            </div>
            <div class="col-xs-12 col-sm-4 text-center">
              <h4>Ética</h4>
              <h5>Transparencia absoluta en nuestro accionar, creando vínculos basados en confianza y honestidad.</h5>
              <h6>•</h6>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-1"></div>
      </div>
    </div>

    <div class="col-xs-12 text-nosotros-bottom">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-1"></div>
          <div class="col-xs-12 col-sm-10 text-center">
            <h3>Staff</h3>
            <p>Somos un equipo de profesionales en Ciencias Económicas con amplia trayectoria en el ejercicio de la profesión, con vasta experiencia en empresas, en todas la áreas (contable, impositiva, previsional y auditoría), adquirida en industrias y estudios de gran importancia a nivel nacional.</p>
          </div>
          <div class="col-xs-12 col-sm-1"></div>
        </div>
      </div>
    </div>

  <?php require_once('includes/footer_home.html'); ?>

